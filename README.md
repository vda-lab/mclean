# MCLEAN

MCLEAN (Multilevel CLustering Exploration As Network) proposes a visual analytics clustering methodology for guiding the user in the exploration and detection of clusters. We thereby combine a graphical representation of the clustered dataset as a network with the community finding algorithms into one coherent framework. Our approach entails displaying the results of the heuristics to users, providing a setting from which to start the exploration and data analysis. This is considered multilevel because allows the user to have an overview and detailed of the merging of elements into clusters.

![Workflow diagram of MCLEAN algorithm. It is summarized in 4 steps: (1) Graph transformation, (2) Node aggregation, (3) Community detection and, (4) Barcode-tree.](http://homes.esat.kuleuven.be/~dalcaide/mclean/img/diagramVersion2.001.jpeg)

The previous diagram displays the workflow of MCLEAN. This approach can be split into four stages:

1. Graph transformation:As hierarchical clustering, the only required input in MCLEAN is the distance matrix. This step transforms the distance matrix into a node-link network. The network relies on parameter ε and this parameter will define the shape of the network. The four small snapshots in [1] are different values of the parameter (ε) for the same dataset.
2. Node aggregation:The resulting networks in [2] are simplifications of the networks obtained in [1] (Graph transformation). The process of simplification is founded on the use of aggregated nodes (meta-nodes) that represent a subgraph that is at a lower level of abstraction.
3. Community detection:Community detection: MCLEAN aims at guiding the identification of substructures through community detection algorithm. Communities detected in a connected component are shown in different color as the snapshots in [3].
4. Barcode-tree:The plot in [4] is a visual representation of clusters arrangement. The individual compounds are arranged along the vertical axis of the plot. At any given threshold, the number of connected components is the number of lines that intersect the vertical line through a threshold. Meta-nodes are formed in the join points that are aggregations of individual data elements or existing meta-nodes at a smaller threshold.

## Installation

To get the current development version from bitbucket:

```R
# install.packages("devtools")
devtools::install_bitbucket("vda-lab/mclean")
```