Package: mclean
Type: Package
Title: Multilevel Clustering Exploration As Network
Version: 0.1.0
Date: 2017-10-29
Author: Daniel Alcaide <daniel.alcaide.villar@gmail.com>
Maintainer: Daniel Alcaide <daniel.alcaide.villar@gmail.com>
Description: MCLEAN (Multilevel CLustering Exploration As Network) proposes a visual 
            analytics clustering methodology for guiding the user in the exploration 
            and detection of clusters. We thereby combine a graphical representation
            of the clustered dataset as a network with the community finding 
            algorithms into one coherent framework. Our approach entails displaying 
            the results of the heuristics to users, providing a setting from which to 
            start the exploration and data analysis. This is considered multilevel
            because allows the user to have an overview and detailed of the merging
            of elements into clusters.
License: GPL (>= 2)
Encoding: UTF-8
LazyData: true
URL: https://github.com/dalcaide/mclean
RoxygenNote: 6.0.1
Imports: igraph, dplyr, reshape2, stats, magrittr, ggplot2
