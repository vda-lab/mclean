#' Add radial area chart inside nodes
#'
#' Combine the input data with mclean output to generate the area radial chart inside the node with plot_network function
#'
#' @param network list; output from \code{mclean} function.
#' @param data matrix or data frame with postitions to add into the \code{network} object.
#' @param labels character string specifying the lables of each data element.
#' @param x character string specifying the x position of each data element.
#' @param y character string specifying the y position of each data element.
#'
#' @return It adds two new columns \code{x} and \code{y} in the \code{node} object.
#'
#' @author Daniel Alcaide, \email{daniel.alcaide@@esat.kuleuven.be}
#' @references Alcaide D, Aerts J. (2018) MCLEAN: Multilevel Clustering Exploration As Network. PeerJ Computer Science 4:e145 \url{https://doi.org/10.7717/peerj-cs.145}
#'
#' @examples
#' data("synthetic_distances")
#' require(Rtsne)
#' tsne <- Rtsne(synthetic_distances, dims = 2, is_distance = TRUE, verbose=FALSE,
#'              perplexity = (nrow(as.matrix(synthetic_distances))/3)-1)
#' tsne_output = cbind(rownames(as.matrix(synthetic_distances)), as.data.frame(tsne$Y));
#' colnames(tsne_output) = c("V1", "V2", "V3")
#' output = mclean(distance_matrix = synthetic_distances, threshold = 187, method = "single")
#' plot_network( add_positions_nodes(output, tsne_output, labels = "V1", x = "V2", y = "V3")  )
#' @export
add_area_chart = function(network, data, same_scale = TRUE ){

  data$id = 1:nrow(data)

  if(same_scale == TRUE) {

    output$area_chart = merge(output$idnodes, data, by = "id") %>%
      dplyr::select(-id, -components, -infomap) %>%
      tidyr::gather(key = "key", value = "value", -name) %>%
      dplyr::mutate(key = as.numeric(as.factor(key)) ) %>%
      dplyr::group_by(key) %>%
      dplyr::mutate(reference = mean (value)) %>%
      dplyr::ungroup(key) %>%
      dplyr::mutate(value = reference - value) %>%
    dplyr::group_by(name, key) %>%
      dplyr::summarize(min = min (value), max = max(value) ) %>%
      tidyr::nest(-name)

  } else {

    output$area_chart = merge(output$idnodes, data, by = "id") %>%
      dplyr::select(-id, -components, -infomap) %>%
      tidyr::gather(key = "key", value = "value", -name) %>%
      dplyr::mutate(key = as.numeric(as.factor(key)) ) %>%
    dplyr::group_by(name, key) %>%
      dplyr::summarize(min = min (value), max = max(value) ) %>%
      tidyr::nest(-name)

  }



  return(output)
}
