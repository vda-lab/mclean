
# Structure of the package

mclean
  | hc_connections
  | grouping_nodes
  | no_grouping_nodes
  | transform_network
  
barcode_tree
  | sort_components
  
data

plot_network
  | networkForceDirected
  | networkFix

plot_barcode_tree
add_positions_nodes
