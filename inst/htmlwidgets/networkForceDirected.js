HTMLWidgets.widget({

    name: "networkForceDirected",
    type: "output",

    initialize: function(el, width, height) {

        d3.select(el).append("svg")
            .attr("width", width)
            .attr("height", height);

        return d3.forceSimulation();
    },

    resize: function(el, width, height, force) {

        d3.select(el).select("svg")
            .attr("width", width)
            .attr("height", height);

        force.force("xAxis", d3.forceX(width / 2))
            .force("yAxis", d3.forceY(height / 2))
            .restart();
    },

    renderValue: function(el, x, force) {

        var net = new network(el, x, force);
        net.defineForce();
        net.defineDragging();
        net.defineDragging();
        net.resetSVG();
        net.drawLinks();
        net.drawingNodes();
        net.forceRestart();
        net.drawCircles();
        net.shinyInteraction();
        net.lassoInteraction();
        net.legend();

    }
});
