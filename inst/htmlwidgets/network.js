
var network  = function(el, x, force, Shiny) {
    this.el = el;
    this.force = force;

    this.options = x.options;
    this.links = HTMLWidgets.dataframeToD3(x.links);
    this.nodes = HTMLWidgets.dataframeToD3(x.nodes);
    this.glyph = x.glyph;

    this.width = el.offsetWidth;
    this.height = el.offsetHeight;

    this.color = eval(this.options.colourScale);
    this.dataForShiny = [];
};

network.prototype.nodeSize = function (d){
    // Compute the node radius  using the javascript math expression specified
    var me = this;
    if(me.options.nodesize){
        return eval(me.options.radiusCalculation);
    } else {
            return 6
    }
};

network.prototype.defineForce = function () {
    var me = this;
    me.force
        .nodes(d3.values(me.nodes))
        .force("link", d3.forceLink(me.links).distance(me.options.linkDistance))
        .force("xAxis", d3.forceX(me.width / 2))
        .force("yAxis", d3.forceY(me.height / 2))
        .force("charge", d3.forceManyBody().strength(me.options.charge))
        .on("tick", tick);

    function tick() {
        me.node.attr("transform", function(d) {
            if(me.options.bounded){ // adds bounding box
                d.x = Math.max(me.nodeSize(d), Math.min(width - me.nodeSize(d), d.x));
                d.y = Math.max(me.nodeSize(d), Math.min(height - me.nodeSize(d), d.y));
            }
            return "translate(" + d.x + "," + d.y + ")"});


        me.link
            .attr("x1", function(d) { return d.source.x; })
            .attr("y1", function(d) { return d.source.y; })
            .attr("x2", function(d) { return d.target.x; })
            .attr("y2", function(d) { return d.target.y; });
    }
};

network.prototype.defineForceFix = function () {
    var me = this;


    me.nodes.forEach(function(d){
        d.x = +d.x;
        d.y = +d.y;
    });


    var margin = [me.width * 0.05, me.height * 0.05];
    var xScale = d3.scaleLinear()
        .domain(d3.extent(me.nodes, function(d) { return d.x; }))
        .range([0 + margin[0], me.width - margin[0]]);

    var yScale = d3.scaleLinear()
        .domain(d3.extent(me.nodes, function(d) { return d.y; }))
        .range([me.height - margin[1], 0 + margin[1]]);

    me.force
        .nodes(d3.values(me.nodes))
        .force("link", d3.forceLink(me.links).distance(me.options.linkDistance))
        .force("xAxis", d3.forceX(function(d) { return xScale(d.x); } ))
        .force("yAxis", d3.forceY(function(d) { return yScale(d.y); } ))
        .force("collide", d3.forceCollide().radius(function(d) { return me.nodeSize(d); }).iterations(2))
        .on("tick", tick);

    function tick() {

        me.node.attr("transform", function(d) {

            if(me.options.bounded){ // adds bounding box
                d.x = Math.max(me.nodeSize(d), Math.min(me.width - me.nodeSize(d), d.x));
                d.y = Math.max(me.nodeSize(d), Math.min(me.height - me.nodeSize(d), d.y));
            }

            return "translate(" + d.x + "," + d.y + ")"});


        me.link
            .attr("x1", function(d) { return d.source.x; })
            .attr("y1", function(d) { return d.source.y; })
            .attr("x2", function(d) { return d.target.x; })
            .attr("y2", function(d) { return d.target.y; });
    }
};



network.prototype.defineDragging = function () {
    var me = this;
    me.drag = d3.drag()
        .on("start", dragstart)
        .on("drag", dragged)
        .on("end", dragended);
    function dragstart(d) {
        if (!d3.event.active) me.force.alphaTarget(0.3).restart();
        d.fx = d.x;
        d.fy = d.y;
    }
    function dragged(d) {
        d.fx = d3.event.x;
        d.fy = d3.event.y;
    }
    function dragended(d) {
        if (!d3.event.active) me.force.alphaTarget(0);
        d.fx = null;
        d.fy = null;
    }
};

network.prototype.resetSVG = function () {
    var me = this;
    me.svg = d3.select(me.el).select("svg");
    me.svg.selectAll("*").remove().append("g");
};

network.prototype.drawLinks = function () {
    var me = this;
    me.link = me.svg.selectAll(".link")
        .data(me.links)
        .enter().append("line")
        .attr("class", "link")
        .style("stroke", function(d) { return d.colour ; })
        .style("opacity", me.options.opacity)
        .style("stroke-width", eval("(" + me.options.linkWidth + ")"))
        .on("mouseover", function(d) {
            d3.select(this)
                .style("opacity", 1);
        })
        .on("mouseout", function(d) {
            d3.select(this)
                .style("opacity", me.options.opacity);
        });
};


network.prototype.drawingNodes = function () {
    var me = this;
    me.node = me.svg.selectAll(".node")
        .data(me.force.nodes())
        .enter().append("g")
        .attr("class", "node")
        .attr("size-pie", function(d){return me.nodeSize(d);})
        .on("mouseover", mouseover)
        .on("mouseout", mouseout)
        .on("click", click)
        .call(me.drag);
    function mouseover() {
        d3.select(this).select("circle").transition()
            .duration(750)
            .attr("r", function(d){return me.nodeSize(d)+5;});
        d3.select(this).select("text").transition()
            .duration(750)
            .attr("x", 13)
            .style("stroke-width", ".5px")
            .style("font", me.options.clickTextSize + "px ")
            .style("opacity", 1);
    }
    function mouseout() {
        d3.select(this).select("circle").transition()
            .duration(750)
            .attr("r", function(d){return me.nodeSize(d);});
        d3.select(this).select("text").transition()
            .duration(1250)
            .attr("x", 0)
            .style("font", me.options.fontSize + "px ")
            .style("opacity", me.options.opacityNoHover);
    }
    function click(d) {
        return eval(me.options.clickAction)
    }
};

network.prototype.forceRestart = function () {
    var me = this;
    // --- Restart the force simulation ---
    // It avoids a bug when we interact with Shiny
    me.force.alphaTarget(0.025).restart();
};

network.prototype.drawCircles = function () {
    var me = this;
    // If the there are more than 1 community the component will be colored.
    // Function to eliminate duplicates
    function eliminateDuplicates(arr) {
        var i,
            len=arr.length,
            out=[],
            obj={};

        for (i=0;i<len;i++) {
            obj[arr[i]]=0;
        }
        for (i in obj) {
            out.push(i);
        }
        return out;
    }
    var countComInComp = d3.nest()
        .key(function(d) { return d.component; })
        .rollup(function(nodes) {
            var groupArray = [];
            Object.keys(nodes).map(function(i) { groupArray.push(nodes[i].group); });
            return eliminateDuplicates(groupArray).length;
        }).entries(me.nodes);


    var colorEval =  function(d) {
        var el = countComInComp.filter(function( e ) {
            return e.key == d.component;
        });
        if (el[0].value > 1) {
            return me.color(d.group);
        } else {
            return "#252525";
        }
    };

    me.node.append("circle")
        .attr("r", function(d){ return me.nodeSize(d);})
        .attr("fill", colorEval)
        .attr("fill-copied", function(d) { return colorEval })
        .attr("class", "node-element")
        .attr("id", function(d) { return "node-" + d.name; })
        .style("stroke", "#000") // Change made: #fff
        .style("opacity", me.options.opacity)
        .style("stroke-width", "1.5px");

    me.node.append("svg:text")
        .attr("class", "nodetext")
        .attr("dx", 12)
        .attr("dy", ".35em")
        .text(function(d) { return d.name; })
        .style("font", me.options.fontSize + "px " + me.options.fontFamily)
        .style("opacity", me.options.opacityNoHover)
        .style("pointer-events", "none");
};


network.prototype.drawAreaChart2 = function () {

    var me = this;

    var min, max, xmin ,xmax = null;

    me.glyph.forEach(function(d,i){


        if (i == 1) {
            min = d3.min(d.data, function(d) {return d.min});
            max = d3.max(d.data, function(d) {return d.max});
            xmin = d3.min(d.data, function(d) {return d.key});
            xmax = d3.max(d.data, function(d) {return d.key});
        } else {

            if (min > d3.min(d.data, function(d) {return d.min}) )  {
                min = d3.min(d.data, function(d) {return d.min});
            }

            if (max < d3.max(d.data, function(d) {return d.max})) {
                max = d3.max(d.data, function(d) {return d.max});
            }

            if (xmax < d3.max(d.data, function(d) {return d.key})) {
                xmax = d3.max(d.data, function(d) {return d.key});
            }

            if (xmin < d3.min(d.data, function(d) {return d.key})) {
                xmin = d3.min(d.data, function(d) {return d.key});
            }

        }
    });

    me.node
        .attr("id", function(d) {return "node-" + d.name})
        .each(function(d,i){

            d3.select(this)
                .append("circle")
                .attr("r", function(d){ return me.nodeSize(d);})
                .attr("fill", "#252525")
                .attr("stroke", "none")
                .style("opacity", me.options.opacity);

            var data = me.glyph[i].data;

            var height = me.nodeSize(d);

            var x = d3.scaleLinear()
                .range([0, 2 * Math.PI])
                .domain([xmin,xmax]);

            var y = d3.scaleRadial()
                .range([0, height])
                .domain([0,max]);

            var areaLow = d3.areaRadial()
                .angle(function(d) { return x(d.key); })
                .innerRadius(y(0))
                .outerRadius(function(d) {
                    if (d.min <= 0) {
                        return y(Math.abs(d.min));
                    } else
                        return y(0)
                });

            var areaHigh = d3.areaRadial()
                .angle(function(d) { return x(d.key); })
                .innerRadius(y(0))
                .outerRadius(function(d) {
                    if (d.min > 0) {
                        return y(Math.abs(d.min));
                    } else
                        return y(0)
                });


            var areaLowMax = d3.areaRadial()
                .angle(function(d) { return x(d.key); })
                .innerRadius(function(d){
                    if (d.min <= 0 ) {
                        return y(Math.abs(d.min))
                    } else {
                        return y(0)
                    }
                })
                .outerRadius(function(d) {
                    if (d.max <= 0 &  d.min <= 0 ) {
                        return y(Math.abs(d.max));
                    } else
                        return y(0)
                });

            var areaHighMax = d3.areaRadial()
                .angle(function(d) { return x(d.key); })
                .innerRadius(function(d){
                    if (d.min > 0 ) {
                        return y(Math.abs(d.min))
                    } else {
                        return y(0)
                    }
                })
                .outerRadius(function(d) {
                    if (d.max > 0 &  d.min > 0 ) {
                        return y(Math.abs(d.max));
                    } else
                        return y(0)
                });

            var areaOtherMax = d3.areaRadial()
                .angle(function(d) { return x(d.key); })
                .innerRadius(function(d){
                    if (d.min <= 0 & d.max > 0 ) {
                        return y(Math.abs(d.min))
                    } else {
                        return y(0)
                    }
                })
                .outerRadius(function(d) {
                    if (d.max > 0 &  d.min <= 0 ) {
                        return y(Math.abs(d.max));
                    } else
                        return y(0)
                });


            ////////////////

            d3.select(this)
                .append("path")
                .datum(data)
                .attr("d", areaLow)
                .attr("class", "area below");

            d3.select(this)
                .append("path")
                .datum(data)
                .attr("d", areaLowMax)
                .attr("class", "area belowmax");

            d3.select(this)
                .append("path")
                .datum(data)
                .attr("d", areaHigh)
                .attr("class", "area above");

            d3.select(this)
                .append("path")
                .datum(data)
                .attr("d", areaHighMax)
                .attr("class", "area abovemax");

            d3.select(this)
                .append("path")
                .datum(data)
                .attr("d", areaOtherMax)
                .attr("class", "area above");




            var yAxis = d3.select(this).append("g")
                .attr("text-anchor", "middle");
            /*
                        var yTick = yAxis
                            .selectAll("g")
                            .data(y.ticks(5))
                            .enter().append("g");

                        yTick.append("circle")
                            .attr("fill", "none")
                            .attr("stroke", "black")
                            .attr("opacity", 0.2)
                            .attr("r", y);

                        yAxis.append("circle")
                            .attr("fill", "none")
                            .attr("stroke", "black")
                            .attr("opacity", 0.2)
                            .attr("r", function() { return y(y.domain()[0])});
            */

        });
};

/*
network.prototype.drawAreaChart = function () {

    var me = this;

    var min, max, xmin ,xmax = null;

    me.glyph.forEach(function(d,i){


        if (i == 1) {
            min = d3.min(d.data, function(d) {return d.min});
            max = d3.max(d.data, function(d) {return d.max});
            xmin = d3.min(d.data, function(d) {return d.key});
            xmax = d3.max(d.data, function(d) {return d.key});
        } else {

            if (min > d3.min(d.data, function(d) {return d.min}) )  {
                min = d3.min(d.data, function(d) {return d.min});
            }

            if (max < d3.max(d.data, function(d) {return d.max})) {
                max = d3.max(d.data, function(d) {return d.max});
            }

            if (xmax < d3.max(d.data, function(d) {return d.key})) {
                xmax = d3.max(d.data, function(d) {return d.key});
            }

            if (xmin < d3.min(d.data, function(d) {return d.key})) {
                xmin = d3.min(d.data, function(d) {return d.key});
            }

        }
    });

    me.node
        .attr("id", function(d) {return "node-" + d.name})
        .each(function(d,i){

            d3.select(this)
                .append("circle")
                .attr("r", function(d){ return me.nodeSize(d);})
                .attr("fill", "#252525")
                .attr("stroke", "none")
                .style("opacity", me.options.opacity);

            var data = me.glyph[i].data;

            var height = me.nodeSize(d);

            var x = d3.scaleLinear()
                .range([0, 2 * Math.PI])
                .domain([xmin,xmax]);

            var y = d3.scaleRadial()
                .range([0, height])
                .domain([min,max]);

            var areaLow = d3.areaRadial()
                .angle(function(d) { return x(d.key); })
                //.innerRadius(function(d) { return y(0n); })
                .outerRadius(function(d) { return y(d.min); });

            var areaHigh = d3.areaRadial()
                .angle(function(d) { return x(d.key); })
                //.innerRadius(function(d) { return y(0n); })
                .outerRadius(function(d) { return y(d.max); });

            var lineMax = d3.areaRadial()
                .angle(function(d) { return x(d.key); })
                .radius(function(d) { return y(d.max); });

            var lineMin = d3.areaRadial()
                .angle(function(d) { return x(d.key); })
                .radius(function(d) { return y(d.min); });

            var lineZero = d3.areaRadial()
                .angle(function(d) { return x(d.key); })
                .radius(y(0));

            d3.select(this)
                .append("clipPath")
                .attr("id", "clip-below-low")
                .append("path")
                .datum(data)
                .attr("d", areaLow.innerRadius(0));

            d3.select(this)
                .append("clipPath")
                .attr("id", "clip-above-low")
                .append("path")
                .datum(data)
                .attr("d", areaLow.innerRadius(height));

            d3.select(this)
                .append("path")
                .datum(data)
                .attr("d", areaLow.innerRadius( y(0) ))
                .attr("clip-path", "url(#clip-above-low)")
                .attr("class", "area above");

            d3.select(this)
                .append("path")
                .datum(data)
                .attr("d", areaLow)
                .attr("clip-path", "url(#clip-below-low)")
                .attr("class", "area below");

            ////////////////
            d3.select(this)
                .append("clipPath")
                .attr("id", "clip-below-high")
                .append("path")
                .datum(data)
                .attr("d", areaHigh.innerRadius(0));

            d3.select(this)
                .append("clipPath")
                .attr("id", "clip-above-high")
                .append("path")
                .datum(data)
                .attr("d", areaHigh.innerRadius(height));

            d3.select(this)
                .append("path")
                .datum(data)
                .attr("d", areaHigh.innerRadius( y(0) ))
                .attr("clip-path", "url(#clip-above-high)")
                .attr("class", "area above");

            d3.select(this)
                .append("path")
                .datum(data)
                .attr("d", areaHigh)
                .attr("clip-path", "url(#clip-below-high)")
                .attr("class", "area below");

            d3.select(this)
                .append("path")
                .datum(data)
                .attr("d", lineMin)
                .attr("class", "lineZero");

            d3.select(this)
                .append("path")
                .datum(data)
                .attr("d", lineMax)
                .attr("class", "line");



            var yAxis = d3.select(this).append("g")
                .attr("text-anchor", "middle");

            var yTick = yAxis
                .selectAll("g")
                .data(y.ticks(5))
                .enter().append("g");

            yTick.append("circle")
                .attr("fill", "none")
                .attr("stroke", "black")
                .attr("opacity", 0.2)
                .attr("r", y);

            yAxis.append("circle")
                .attr("fill", "none")
                .attr("stroke", "black")
                .attr("opacity", 0.2)
                .attr("r", function() { return y(y.domain()[0])});


        })
};
*/




network.prototype.drawPieChart = function () {
    var me = this;

    var pie = d3.pie()
        .sort(null)
        .value(function(d) { return d; });

    node
        .attr("id", function(d) {return "node-" + d.name})
        .selectAll(".arc")
        .data(function(d,i){ return pie(x.categories[i]) }).enter()
        .append("path")
        .attr("d", d3.arc()
            .outerRadius(function(d,i){
                nodesize = d3.select(this.parentNode).attr("size-pie");
                return nodesize;
            }).innerRadius(0)
        ).attr("fill", function(d) { return color(d.index); })
        .attr("fill-copied", function(d) { return color(d.index); })
        .attr("class", "node-element")
        .style("opacity", options.opacity);

    me.node.append("svg:text")
        .attr("class", "nodetext")
        .attr("dx", 12)
        .attr("dy", ".35em")
        .text(function(d) { return d.name; })
        .style("font", me.options.fontSize + "px " + me.options.fontFamily)
        .style("opacity", me.options.opacityNoHover)
        .style("pointer-events", "none");
};





network.prototype.shinyInteraction = function () {
    var me = this;
    if (me.options.shiny) {
        // Add id name to svg
        d3.select(me.el).select("svg").attr("id", me.options.id);

        // <-- NOTE: Adding the id is a way to identify the source of this data in shiny

        function receiveDataFromShiny(dataReceived) {
            for (var key in dataReceived) {
                console.log(key, dataReceived[key]);
                var dataSelected = [].concat(dataReceived[key]);
                // <-- Avoid problems when there is only one node
                // Restore previous color
                d3.select("#" + key).selectAll(".node-element").attr("fill", function (d) {
                    return d3.select(this).attr("fill-copied")
                });
                // Select new data
                dataSelected.forEach(function (d) {
                    d3.select("#" + key).select("#node-" + d).attr("fill", "yellow");
                });
            }
        }

        Shiny.addCustomMessageHandler("myCallbackHandler", receiveDataFromShiny);
    }
};

network.prototype.lassoInteraction = function () {
    var me = this;
    // Lasso sub-functions
    var lasso_start = function () {
        lasso.items()
            .classed("not_possible", true)
            .classed("selected", false);
    };
    var lasso_draw = function () {
        // Style the possible dots
        lasso.possibleItems()
            .classed("not_possible", false)
            .classed("possible", true);
        // Style the not possible dot
        lasso.notPossibleItems()
            .classed("not_possible", true)
            .classed("possible", false);
    };
    var lasso_end = function () {
        // Reset the color of all dots
        lasso.items()
            .classed("not_possible", false)
            .classed("possible", false);
        // Style the selected dots
        lasso.selectedItems()
            .classed("selected", true);
        // Data for Shiny
        dataForShiny = [];
        lasso.selectedItems().each(function (d) {
            dataForShiny.push(d.name);
        });
        Shiny.onInputChange("mydata", sendDataToShiny(dataForShiny));
    };
    // Lasso function
    var lasso = d3.lasso()
        .closePathSelect(true)
        .closePathDistance(150)
        .items(me.node.selectAll(".node-element"))
        .targetArea(d3.select(me.el).select("svg"))
        .on("start", lasso_start)
        .on("draw", lasso_draw)
        .on("end", lasso_end);

    d3.select(me.el).select('svg').call(lasso);
};

network.prototype.legend = function () {
    var me = this;
    // add legend option
    if(me.options.legend){
        var legendRectSize = 18;
        var legendSpacing = 4;
        var legend = svg.selectAll('.legend')
            .data(me.color.domain())
            .enter()
            .append('g')
            .attr('class', 'legend')
            .attr('transform', function(d, i) {
                var height = legendRectSize + legendSpacing;
                var offset =  height * me.color.domain().length / 2;
                var horz = legendRectSize;
                var vert = i * height+4;
                return 'translate(' + horz + ',' + vert + ')';
            });

        legend.append('rect')
            .attr('width', legendRectSize)
            .attr('height', legendRectSize)
            .style('fill', color)
            .style('stroke', color);

        legend.append('text')
            .attr('x', legendRectSize + legendSpacing)
            .attr('y', legendRectSize - legendSpacing)
            .text(function(d) { return d; });

        // make font-family consistent across all elements
        d3.select(me.el).selectAll('text').style('font-family', me.options.fontFamily);
    }

};
